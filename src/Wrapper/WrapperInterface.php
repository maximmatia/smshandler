<?php

namespace SmsHandler\Wrapper;

use SmsHandler\Provider\ProviderInterface;

interface WrapperInterface extends ProviderInterface
{
    /**
     * Fetch number
     *
     * @param array $options
     *
     * @return mixed
     */
#    public function fetchNumber(array $options);

    public function getNumberAmount(array $options = []);

    public function getCode($phoneNumber);
}

