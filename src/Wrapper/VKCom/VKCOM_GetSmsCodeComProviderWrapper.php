<?php

namespace SmsHandler\Wrapper\VKCom;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\TemplateException;
use SmsHandler\Wrapper\AbstractWrapper;

class VKCOM_GetSmsCodeComProviderWrapper extends AbstractWrapper
{
    /**
     * @param $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToFetchNumber(array $options = []): array
    {
        if (!array_key_exists('country', $options)) {
            throw new ConfigException('Country is not set');
        }

        return ['serviceId' => 17, 'country' => $options['country']];
    }

    public function getOptionsToGetNumberAmount(array $options)
    {
        // TODO: Implement getOptionsToGetNumberAmount() method.
    }

    /**
     * @param $response
     *
     * @return int
     */
    public function parseNumberAmount($response)
    {
        return 1;
    }

    /**
     * @param $smsContent
     *
     * @return mixed
     * @throws TemplateException
     */
    public function parseCode($smsContent)
    {
        preg_match('/Code ([0-9a-z]{4,9})/i', $smsContent, $code);
        if (!$code[1]) {
            preg_match('/VK: ([0-9a-z]{4,9})/i', $smsContent, $code);
        }

        if ($code[1]) {
            return $code[1];
        } else {
            throw new TemplateException('Unknown sms template');
        }
    }
}

