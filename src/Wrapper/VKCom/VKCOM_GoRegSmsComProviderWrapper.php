<?php

namespace SmsHandler\Wrapper\VKCom;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Exception\TemplateException;
use SmsHandler\Wrapper\AbstractWrapper;

class VKCOM_GoRegSmsComProviderWrapper extends AbstractWrapper
{
    const SUPPORTED_COUNTRIES = [
        'az' => 'azerbaijan', # azerbaijan,
        'ht' => 'haiti', # haiti,
        'gm' => 'gambia', # gambia,
        'gh' => 'ghana', # ghana,
        'eg' => 'egypt', # egypt,
        'iq' => 'iraq', # iraq
        'ye' => 'yemen', # yemen,
        'cm' => 'cameroon', # cameroon
        'ci' => 'ivory', # ivory coast,
        'ng' => 'nigeria', # nigeria,
        'rs' => 'serbia', # serbia,
        'uz' => 'uzbekistan', # uzbekistan
        'td' => 'chad', # chad
        'gq' => 'guinea', # guinea
        'ml' => 'mali', # mali
        'kz' => 'kazakhstan', # kazakhstan

        # https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes
    ];

    /**
     * @param $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToFetchNumber(array $options = []): array
    {
        if (!array_key_exists('country', $options) AND !array_key_exists('countryRandom', $options)) {
            throw new ConfigException('Country is not set');
        }

        if (array_key_exists('countryRandom', $options)) {
            $options['country'] = $options['countryRandom'][array_rand($options['countryRandom'])];
            unset($options['countryRandom']);
        }

        if (array_key_exists($options['country'], self::SUPPORTED_COUNTRIES)) {
            $countryCode = self::SUPPORTED_COUNTRIES[strtolower($options['country'])];
        } elseif (array_key_exists('country', @$this->options['supportedCountries'])) {
            $countryCode = $this->options['supportedCountries']['country'];
        } else {
            throw new ConfigException('Country is not supported');
        }

        return ['serviceId' => 'vk', 'country' => $countryCode];
    }

    /**
     * @param array $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToGetNumberAmount(array $options)
    {
        if (!array_key_exists('country', $options)) {
            throw new ConfigException('Country is not set');
        }

        if (array_key_exists($options['country'], self::SUPPORTED_COUNTRIES)) {
            $countryCode = self::SUPPORTED_COUNTRIES[strtolower($options['country'])];
        } elseif (array_key_exists('country', @$this->options['supportedCountries'])) {
            $countryCode = $this->options['supportedCountries']['country'];
        } else {
            throw new ConfigException('Country is not supported');
        }

        return ['country' => $countryCode];
    }

    /**
     * @param $response
     *
     * @return int
     */
    public function parseNumberAmount($response)
    {
        return 10;
    }

    /**
     * @param $smsContent
     *
     * @return mixed
     */
    public function parseCode($smsContent)
    {
        preg_match('/Code ([0-9a-z]{4,9})/i', $smsContent, $code);
        if (!$code[1]) {
            preg_match('/VK: ([0-9a-z]{4,9})/i', $smsContent, $code);
        }

        if ($code[1]) {
            return $code[1];
        } else {
            throw new TemplateException('Unknown sms template');
        }
    }
}

