<?php

namespace SmsHandler\Wrapper;

use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Provider\AbstractProvider;

abstract class AbstractWrapper implements WrapperInterface
{
    protected $provider;
    protected $options;

    abstract public function getOptionsToFetchNumber(array $options);

    abstract public function getOptionsToGetNumberAmount(array $options);

    abstract public function parseCode($smsContent);

    abstract public function parseNumberAmount($response);

    /**
     * AbstractWrapper constructor.
     *
     * @param AbstractProvider $provider
     * @param array            $options
     */
    public function __construct(AbstractProvider $provider, array $options)
    {
        $this->provider = $provider;
        $this->options = $options;
    }

    /**
     * @return AbstractProvider
     */
    public function getProvider(): AbstractProvider
    {
        return $this->provider;
    }

    /**
     * @param array $options
     *
     * @return mixed
     * @throws \SmsHandler\Exception\ProviderRuntimeException
     */
    public function fetchNumber(array $options = [])
    {
        for ($i = 0; $i < $this->getProvider()->getConfig('fetchNumberAttempts'); $i++) {
            try {
                $providerOptions = $this->getOptionsToFetchNumber($options);

                return $this->getProvider()->fetchNumber($providerOptions);
            } catch (ProviderRuntimeException $e) {
                if (in_array($e->getCode(), [
                    ProviderRuntimeException::NO_NUMBERS,
                ])) {
                    continue;
                }

                throw $e;
            }
        }

        throw new ProviderRuntimeException('Phone number is not fetched', 0, (isset($e) ? $e : null));
    }

    public function blockNumber($phoneNumber): bool
    {
        return $this->getProvider()->blockNumber($phoneNumber);
    }

    public function getSms($phoneNumber)
    {
        return $this->getProvider()->getSms($phoneNumber);
    }

    public function resendSms($phoneNumber)
    {
        return $this->getProvider()->resendSms($phoneNumber);
    }

    public function releaseNumber($phoneNumber): bool
    {
        return $this->getProvider()->releaseNumber($phoneNumber);
    }

    public function getBalance()
    {
        return $this->getProvider()->getBalance();
    }

    /**
     * @param $phoneNumber
     *
     * @return mixed
     * @throws \SmsHandler\Exception\ProviderRuntimeException
     */
    public function getCode($phoneNumber)
    {
        $sms = $this->getProvider()->getSms($phoneNumber);

        return $this->parseCode($sms);
    }

    /**
     * @param array $options
     *
     * @return mixed
     */
    public function getNumberAmount(array $options = [])
    {
        $providerOptions = $this->getOptionsToGetNumberAmount($options);
        $result = $this->getProvider()->getNumberAmount($providerOptions);

        return $this->parseNumberAmount($result);
    }

    public function getPhoneNumbers(): array
    {
        return $this->getProvider()->getPhoneNumbers();
    }

    public function getPhoneNumber()
    {
        return $this->getProvider()->getPhoneNumber();
    }

    public function deleteSms($phoneNumber)
    {
        $this->getProvider()->deleteSms($phoneNumber);
    }

}

