<?php

namespace SmsHandler\Wrapper\LinkedinCom;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Exception\TemplateException;
use SmsHandler\Wrapper\AbstractWrapper;

class LinkedinCom_SmsHubOrgProviderWrapper extends AbstractWrapper
{
    const SUPPORTED_COUNTRIES = [
        'ru' => 0, # russia
        'ua' => 1, # ukraine
        'kz' => 2, # kazakhstan
        'cn' => 3, # china
        'ph' => 4, # philippines
        'mm' => 5, # myanmar,
        'id' => 6, # indonesia,
        'my' => 7, # malaysia,
        'ke' => 8, # kenya,
        'tz' => 9, # tanzania,
        'vn' => 10, # vietnam,
        'kg' => 11, # kirghizstan,
        'us' => 12, # usa,
        'il' => 13, # israel,
        'hk' => 14, # hong kong,
        'pl' => 15, # poland,
        'gb' => 16, # united kingdom,
        'mg' => 17, # madagascar,
        'ng' => 19, # nigeria,
        'mo' => 20, # macao,
        'eg' => 21, # egypt,
        'ie' => 23, # ireland,
        'kh' => 24, # cambodia,
        'la' => 25, # lao,
        'ht' => 26, # haiti,
        'ci' => 27, # ivory coast,
        'gm' => 28, # gambia,
        'rs' => 29, # serbia,
        'ye' => 30, # yemen,
        'za' => 31, # south africa,
        'ro' => 32, # romania,
        'ee' => 34, # estonia,
        'az' => 35, # azerbaijan,
        'ca' => 36, # canada,
        'ma' => 37, # morocco,
        'gh' => 38, # ghana,
        'ar' => 39, # argentina,
        'uz' => 40, # uzbekistan
        'cm' => 41, # cameroon
        'td' => 42, # chad
        'de' => 43, # germany
        'lt' => 44, # lithuania
        'hr' => 45, # croatia
        'iq' => 47, # iraq
        'nl' => 48, # netherlands
        'lv' => 49, # latvia
        'at' => 50, # austria
        'by' => 51, # belarus
        'th' => 52, # thailand
        'sa' => 53, # saudi arabia
        'si' => 59, # slovenia
        'cz' => 53, # czechia
        'nz' => 67, # new zealand
        'gq' => 68, # guinea
        'ml' => 69, # mali
        'mn' => 72, # mongolia
        'br' => 73, # brazil

        # https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes
    ];

    /**
     * @param $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToFetchNumber(array $options = []): array
    {
        if (!array_key_exists('country', $options) AND !array_key_exists('countryRandom', $options)) {
            throw new ConfigException('Country is not set');
        }

        if (array_key_exists('countryRandom', $options)) {
            $options['country'] = $options['countryRandom'][array_rand($options['countryRandom'])];
            unset($options['countryRandom']);
        }

        if (array_key_exists($options['country'], self::SUPPORTED_COUNTRIES)) {
            $countryCode = self::SUPPORTED_COUNTRIES[strtolower($options['country'])];
        } elseif (array_key_exists('country', @$this->options['supportedCountries'])) {
            $countryCode = $this->options['supportedCountries']['country'];
        } else {
            throw new ConfigException('Country is not supported');
        }

        return ['serviceId' => 'tn', 'country' => $countryCode];
    }

    /**
     * @param array $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToGetNumberAmount(array $options)
    {
        if (!array_key_exists('country', $options)) {
            throw new ConfigException('Country is not set');
        }

        if (array_key_exists($options['country'], self::SUPPORTED_COUNTRIES)) {
            $countryCode = self::SUPPORTED_COUNTRIES[strtolower($options['country'])];
        } elseif (array_key_exists('country', @$this->options['supportedCountries'])) {
            $countryCode = $this->options['supportedCountries']['country'];
        } else {
            throw new ConfigException('Country is not supported');
        }

        return ['country' => $countryCode];
    }

    /**
     * @param $response
     *
     * @return array
     * @throws ProviderRuntimeException
     * @throws TemplateException
     */
    public function parseNumberAmount($response)
    {
        if (!is_array($response)) {
            throw new TemplateException('Parsing error');
        } elseif (!array_key_exists('tn_0', $response)) {
            throw new ProviderRuntimeException(ProviderRuntimeException::BAD_RESPONSE);
        }

        return $response['tn_0'];
    }

    /**
     * @param $smsContent
     *
     * @return mixed
     */
    public function parseCode($smsContent)
    {
        return $smsContent;
    }
}