<?php

namespace SmsHandler\Wrapper\GoogleCom;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Exception\TemplateException;
use SmsHandler\Wrapper\AbstractWrapper;

class GoogleCom_VakSmsComProviderWrapper extends AbstractWrapper
{
    /**
     * @param array $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToFetchNumber(array $options = []): array
    {
        if (!isset($options['country'])) {
            $options['country'] = 'ru';
        }

        return ['serviceId' => 'gl', 'country' => strtolower($options['country'])];
    }

    /**
     * @param array $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToGetNumberAmount(array $options)
    {
        if (!isset($options['country'])) {
            $options['country'] = 'ru';
        }

        return ['serviceId' => 'gl', 'country' => strtolower($options['country'])];
    }

    /**
     * @param $response
     *
     * @return array
     * @throws ProviderRuntimeException
     * @throws TemplateException
     */
    public function parseNumberAmount($response)
    {
        if (!is_array($response)) {
            throw new TemplateException('Parsing error');
        }

        if (isset($response['gl'])) {
            return $response['gl'];
        }

        throw new ProviderRuntimeException(ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $smsContent
     *
     * @return mixed
     */
    public function parseCode($smsContent)
    {
        return $smsContent;
    }
}

