<?php

namespace SmsHandler\Wrapper\GoogleCom;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Exception\TemplateException;
use SmsHandler\Wrapper\AbstractWrapper;

class GoogleCom_SmsAcktiwatorRuProviderWrapper extends AbstractWrapper
{
    /**
     * @param array $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToFetchNumber(array $options = []): array
    {
        if (!isset($options['country'])) {
            throw new ConfigException('Country is not supported');
        }

        return ['serviceId' => '9', 'code' => strtoupper($options['country'])];
    }

    /**
     * @param array $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToGetNumberAmount(array $options)
    {
        if (!isset($options['country'])) {
            throw new ConfigException('Country is not supported');
        }

        return ['code' => strtoupper($options['country'])];
    }

    /**
     * @param $response
     *
     * @return array
     * @throws ProviderRuntimeException
     * @throws TemplateException
     */
    public function parseNumberAmount($response)
    {
        if (!is_array($response)) {
            throw new TemplateException('Parsing error');
        }

        foreach ($response as $item) {
            if ($item['id'] == 9) {
                return $item['count'];
            }
        }

        throw new ProviderRuntimeException(ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $smsContent
     *
     * @return mixed
     */
    public function parseCode($smsContent)
    {
        return $smsContent;
    }
}

