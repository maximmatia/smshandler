<?php

namespace SmsHandler\Wrapper\HotmailCom;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Exception\TemplateException;
use SmsHandler\Provider\SmsHubOrgProvider;
use SmsHandler\Wrapper\AbstractWrapper;

class HotmailCom_SmsHubOrgProviderWrapper extends AbstractWrapper
{
    /**
     * @param array $options
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToFetchNumber(array $options = []): array
    {
        if (!array_key_exists('country', $options) AND !array_key_exists('countryRandom', $options)) {
            throw new ConfigException('Country is not set');
        }

        if (array_key_exists('countryRandom', $options)) {
            $options['country'] = $options['countryRandom'][array_rand($options['countryRandom'])];
            unset($options['countryRandom']);
        }

        if (array_key_exists($options['country'], SmsHubOrgProvider::SUPPORTED_COUNTRIES)) {
            $countryCode = SmsHubOrgProvider::SUPPORTED_COUNTRIES[strtolower($options['country'])];
        } elseif (array_key_exists('country', @$this->options['supportedCountries'])) {
            $countryCode = $this->options['supportedCountries']['country'];
        } else {
            throw new ConfigException('Country is not supported');
        }

        return ['serviceId' => 'mm', 'country' => $countryCode];
    }

    /**
     * @param array $options
     *
     * @return array
     * @throws ConfigException
     */
    public function getOptionsToGetNumberAmount(array $options)
    {
        if (!array_key_exists('country', $options)) {
            throw new ConfigException('Country is not set');
        }

        if (array_key_exists($options['country'], SmsHubOrgProvider::SUPPORTED_COUNTRIES)) {
            $countryCode = SmsHubOrgProvider::SUPPORTED_COUNTRIES[strtolower($options['country'])];
        } elseif (array_key_exists('country', @$this->options['supportedCountries'])) {
            $countryCode = $this->options['supportedCountries']['country'];
        } else {
            throw new ConfigException('Country is not supported');
        }

        return ['country' => $countryCode];
    }

    /**
     * @param $response
     *
     * @return array
     * @throws ProviderRuntimeException
     * @throws TemplateException
     */
    public function parseNumberAmount($response)
    {
        if (!is_array($response)) {
            throw new TemplateException('Parsing error');
        } elseif (!array_key_exists('mm_0', $response)) {
            throw new ProviderRuntimeException(ProviderRuntimeException::BAD_RESPONSE);
        }

        return $response['mm_0'];
    }

    /**
     * @param $smsContent
     *
     * @return mixed
     */
    public function parseCode($smsContent)
    {
        return $smsContent;
    }
}
