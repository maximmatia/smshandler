<?php

namespace SmsHandler\Util\Response;

use HttpClient\Response;

class SmsAcktiwatorRuResponse extends Response
{
    public function isNoBalance()
    {
        if ($this->isJson()) {
            $pr = $this->parseJson(1);
            if (isset($pr['name']) and $pr['name'] == 'error' and isset($pr['code']) and $pr['code'] == '102') {
                return true;
            }
        }

        return false;
    }

    public function isNoNumbers()
    {
        if ($this->isJson()) {
            $pr = $this->parseJson(1);
            if (isset($pr['name']) and $pr['name'] == 'error' and isset($pr['code']) and $pr['code'] == '103') {
                return true;
            }
        }

        return false;
    }


    public function isRequestError()
    {
        if ($this->isJson()) {
            $pr = $this->parseJson(1);
            if (isset($pr['name']) and $pr['name'] == 'error' and isset($pr['code'])) {
                return true;
            } elseif (isset($pr['status']) and $pr['status'] !== 200) {
                return true;
            }
        }

        return false;
    }

    public function isReleasePhoneError(): bool
    {
        if ($this->isJson()) {
            $pr = $this->parseJson(1);
            if (isset($pr['error']) and substr_count($pr['error'], 'Возврат не возможен сразу после взятия')) {
                return true;
            }
        }

        return false;
    }
}

