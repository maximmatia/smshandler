<?php

namespace SmsHandler\Util\Response;

use HttpClient\Response;

class GoRegSmsComResponse extends Response
{
    public function isNoBalance()
    {
        if ($this->getBody() == 'NO_BALANCE') {
            return true;
        }

        return false;
    }

    public function isNoNumbers()
    {
        if ($this->getBody() == 'NO_NUMBERS') {
            return true;
        }

        return false;
    }

}

