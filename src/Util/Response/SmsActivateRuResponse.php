<?php

namespace SmsHandler\Util\Response;

use HttpClient\Response;

class SmsActivateRuResponse extends Response
{
    public function isNoBalance()
    {
        if ($this->getBody() == 'NO_BALANCE') {
            return true;
        }

        return false;
    }

    public function isNoNumbers()
    {
        if ($this->getBody() == 'NO_NUMBERS') {
            return true;
        }

        return false;
    }


    public function isRequestError()
    {
        if (in_array($this->getBody(), [
            'NO_ACTIVATION',
            'BAD_STATUS',
            'BAD_SERVICE',
            'BAD_ACTION',
            'BAD_KEY',
        ])) {
            return true;
        }

        return false;
    }


    public function isInternalError()
    {
        if (in_array($this->getBody(), ['ERROR_SQL'])) {
            return true;
        }

        return false;
    }

}

