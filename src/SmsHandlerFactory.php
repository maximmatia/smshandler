<?php

namespace SmsHandler;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Provider\AbstractProvider;
use SmsHandler\Provider\DropsmsRuProvider;
use SmsHandler\Provider\GetSmsCodeComProvider;
use SmsHandler\Provider\GoRegSmsComProvider;
use SmsHandler\Provider\SmsAcktiwatorRuProvider;
use SmsHandler\Provider\SmsActivateRuProvider;
use SmsHandler\Provider\SmsHubOrgProvider;
use SmsHandler\Provider\VakSmsComProvider;

class SmsHandlerFactory
{
    /**
     * @param       $providerDomain
     * @param array $config
     *
     * @return SmsActivateRuProvider
     * @throws Exception\ConfigException
     */
    public static function init($providerDomain, array $config): AbstractProvider
    {
        $providerDomain = str_replace('www.', '', strtolower($providerDomain));
        if ($providerDomain == 'sms-activate.ru') {
            return new SmsActivateRuProvider($config);
        } elseif ($providerDomain == 'smshub.org') {
            return new SmsHubOrgProvider($config);
        } elseif ($providerDomain == 'getsmscode.com') {
            return new GetSmsCodeComProvider($config);
        } elseif ($providerDomain == 'sms-acktiwator.ru') {
            return new SmsAcktiwatorRuProvider($config);
        } elseif ($providerDomain == 'goreg-sms.com') {
            return new GoRegSmsComProvider($config);
        } elseif ($providerDomain == 'vak-sms.com') {
            return new VakSmsComProvider($config);
        } elseif ($providerDomain == 'dropsms.ru') {
            return new DropsmsRuProvider($config);
        }

        throw new ConfigException('Provider is not supported.');
    }
}

