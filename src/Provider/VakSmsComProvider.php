<?php

namespace SmsHandler\Provider;

use GuzzleHttp\Exception\GuzzleException;
use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Wrapper\AbstractWrapper;
use SmsHandler\Wrapper\GoogleCom\GoogleCom_VakSmsComProviderWrapper;
use SmsHandler\Wrapper\HotmailCom\HotmailCom_VakSmsComProviderWrapper;
use SmsHandler\Wrapper\LinkedinCom\LinkedinCom_VakSmsComProviderWrapper;
use SmsHandler\Wrapper\TwitterCom\TwitterCom_VakSmsComProviderWrapper;
use SmsHandler\Wrapper\VKCom\VKCOM_VakSmsComProviderWrapper;

class VakSmsComProvider extends AbstractProvider
{
    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->httpClient->setConfigOption('base_uri', 'https://vak-sms.com');
    }

    /**
     * @param       $serviceDomain
     * @param array $options
     *
     * @return AbstractWrapper
     * @throws ConfigException
     */
    public function __invoke($serviceDomain, array $options = []): AbstractWrapper
    {
        $serviceDomain = str_replace('www.', '', strtolower($serviceDomain));
        if ($serviceDomain == 'vk.com') {
            return new VKCOM_VakSmsComProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'linkedin.com') {
            return new LinkedinCom_VakSmsComProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'twitter.com') {
            return new TwitterCom_VakSmsComProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'google.com') {
            return new GoogleCom_VakSmsComProviderWrapper($this, $options);
        }elseif ($serviceDomain == 'hotmail.com') {
            return new HotmailCom_VakSmsComProviderWrapper($this, $options);
        }


        throw new ConfigException('Provider is not supported.');
    }

    /**
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function getBalance()
    {
        $r = $this->httpClient->request('/api/getBalance/')
            ->addQuery('apiKey', $this->getConfig('apiKey'))
            ->getResponse();

        if (!$r->isJson()) {
            throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
        }

        $pr = $r->parseJson(true);
        if (isset($pr['balance'])) {
            return $pr['balance'];
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param array $options
     *
     * @return mixed
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     * @throws ConfigException
     */
    public function getNumberAmount(array $options = [])
    {
        if (!array_key_exists('serviceId', $options)) {
            throw new ConfigException('service is not set');
        }

        $serviceId = $options['serviceId'];
        unset($options['serviceId']);

        $request = $this->httpClient->request('/api/getCountNumber/')
            ->addQuery('service', $serviceId)
            ->addQuery('apiKey', $this->getConfig('apiKey'));
        foreach ($options as $k => $v) {
            $request->addQuery($k, $v);
        }
        $r = $request->getResponse();

        if (!$r->isJson()) {
            throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
        }

        return $r->parseJson(true);
    }


    /**
     * @param array $options
     *
     * @return mixed
     * @throws ConfigException
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function fetchNumberRequest(array $options)
    {
        if (!array_key_exists('serviceId', $options)) {
            throw new ConfigException('service is not set');
        }

        $serviceId = $options['serviceId'];
        unset($options['serviceId']);

        $request = $this->httpClient->request('/api/getNumber/')
            ->addQuery('service', $serviceId)
            ->addQuery('apiKey', $this->getConfig('apiKey'));
        foreach ($options as $k => $v) {
            $request->addQuery($k, $v);
        }
        $r = $request->getResponse();

        if (!$r->isJson()) {
            throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
        }

        $pr = $r->parseJson(1);
        if (isset($pr['error'])) {
            switch ($pr['error']) {
                case 'noNumber':
                    throw new ProviderRuntimeException(
                        'No phone numbers available.',
                        ProviderRuntimeException::NO_NUMBERS
                    );
                case 'noMoney':
                    throw new ProviderRuntimeException('Zero balance.', ProviderRuntimeException::NO_BALANCE);
                default:
                    throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
            }
        }

        if (isset($pr['tel']) and isset($pr['idNum'])) {
            $this->deletePhoneData($pr['tel']);
            $this->setPhoneData($pr['tel'], [
                'phoneId'     => $pr['idNum'],
                'phoneNumber' => $pr['tel'],
                'serviceId'   => $serviceId,
            ]);

            return $pr['tel'];
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function getSmsRequest($phoneNumber)
    {
        $r = $this->httpClient->request('/api/getSmsCode/')
            ->addQuery('apiKey', $this->getConfig('apiKey'))
            ->addQuery('idNum', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse();

        if (!$r->isJson()) {
            throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
        }

        $pr = $r->parseJson(1);
        if (array_key_exists('smsCode', $pr)) {
            if (is_null($pr['smsCode'])) {
                return null;
            }

            return $this->smsStorage[$phoneNumber] = $pr['smsCode'];
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function releaseNumber($phoneNumber): bool
    {
        return $this->setStatus($phoneNumber, 'end');
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function blockNumber($phoneNumber): bool
    {
        return $this->setStatus($phoneNumber, 'bad');
    }

    /**
     * @param $phoneNumber
     *
     * @return bool|mixed
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function resendSms($phoneNumber)
    {
        return $this->setStatus($phoneNumber, 'send');
    }

    protected function setStatus($phoneNumber, $status)
    {
        $r = $this->httpClient->request('/api/setStatus/')
            ->addQuery('apiKey', $this->getConfig('apiKey'))
            ->addQuery('idNum', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->addQuery('status', $status)
            ->getResponse();

        if (!$r->isJson()) {
            throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
        }

        $pr = $r->parseJson(1);
        if (isset($pr['status'])) {
            switch ($pr['status']) {
                case 'ready':
                case 'update':
                    return true;
                case 'waitSMS':
                case 'smsReceived':
                    return false;
                default:
                    throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
            }
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }
}

