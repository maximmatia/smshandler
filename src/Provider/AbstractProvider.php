<?php

namespace SmsHandler\Provider;

use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use HttpClient\HttpClient;
use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Wrapper\AbstractWrapper;

abstract class AbstractProvider implements ProviderInterface
{
    protected $httpClient;
    protected $httpClientLog = [];
    protected $config;
    protected $phoneData     = [];
    protected $smsStorage    = [];

    /**
     * Init wrapper for specified service domain
     *
     * @param       $serviceDomain
     * @param array $options
     *
     * @return AbstractWrapper
     */
    abstract public function __invoke($serviceDomain, array $options = []): AbstractWrapper;

    /**
     * Get sms attempt
     *
     * @param $phoneNumber
     *
     * @return mixed
     */
    abstract public function getSmsRequest($phoneNumber);

    abstract public function fetchNumberRequest(array $options);

    /**
     * AbstractProvider constructor.
     *
     * @param array $config
     *
     * @throws ConfigException
     */
    public function __construct(array $config)
    {
        if (!array_key_exists('apiKey', $config)) {
            throw new ConfigException('apiKey is not set.');
        }

        if (!array_key_exists('smsWaitingForInterval', $config)) {
            $config['smsWaitingForInterval'] = 300;
        }
        if (!array_key_exists('smsLoopSleepInterval', $config)) {
            $config['smsLoopSleepInterval'] = 5;
        }
        if (!array_key_exists('fetchNumberAttempts', $config)) {
            $config['fetchNumberAttempts'] = 1;
        }

        if (isset($config['phoneData'])) {
            $this->phoneData = $config['phoneData'];
            unset($config['phoneData']);
        }

        $this->setConfig($config);


        $stack = HandlerStack::create();
        $stack->push(Middleware::history($this->httpClientLog));
        $this->httpClient = new HttpClient(
            [
                'cookies'         => [],
                'decode_content'  => 'gzip',
                'verify'          => false,
                'timeout'         => 60,
                'connect_timeout' => 10,
                'http_errors'     => false,
                'allow_redirects' => false,
                'connectLimits'   => [
                    'attempts' => 3,
                ],
            ] + (isset($config['proxy']) ? ['proxy' => $config['proxy']] : [])
        );
    }

    /**
     * fetch phone number
     *
     * @param array $options
     *
     * @return mixed
     * @throws ProviderRuntimeException
     */
    public function fetchNumber(array $options)
    {
        for ($i = 0; $i < $this->getConfig('fetchNumberAttempts'); $i++) {
            try {
                return $this->fetchNumberRequest($options);
            } catch (ProviderRuntimeException $e) {
                if (in_array($e->getCode(), [
                    ProviderRuntimeException::INTERNAL_ERROR,
                ])) {
                    continue;
                }

                throw $e;
            }
        }

        throw new ProviderRuntimeException('Phone number is not fetched', 0, (isset($e) ? $e : null));
    }

    /**
     * Get sms loop
     *
     * @param $phoneNumber
     *
     * @return mixed
     * @throws ProviderRuntimeException
     */
    public function getSms($phoneNumber)
    {
        if (array_key_exists($phoneNumber, $this->smsStorage)) {
            return $this->smsStorage[$phoneNumber];
        } else {
            $startTimestamp = time();
            while (time() - $startTimestamp < $this->getConfig('smsWaitingForInterval')) {
                $sms = $this->getSmsRequest($phoneNumber);
                if (!is_null($sms)) {
                    return $sms;
                } else {
                    sleep($this->getConfig('smsLoopSleepInterval'));
                }
            }

            throw new ProviderRuntimeException('No sms accepted', ProviderRuntimeException::NO_SMS);
        }
    }

    public function getPhoneNumbers(): array
    {
        return array_keys($this->phoneData);
    }

    public function getPhoneNumber()
    {
        return array_key_first($this->phoneData);
    }

    /**
     * Get phone data
     *
     * @param      $phoneNumber
     * @param null $paramName
     *
     * @return mixed
     * @throws ProviderRuntimeException
     */
    public function getPhoneData($phoneNumber, $paramName = null)
    {
        if (array_key_exists($phoneNumber, $this->phoneData)) {
            if (!is_null($paramName)) {
                if (array_key_exists($paramName, $this->phoneData[$phoneNumber])) {
                    return $this->phoneData[$phoneNumber][$paramName];
                } else {
                    throw new ProviderRuntimeException('Phone data param is not set.');
                }
            } else {
                return $this->phoneData[$phoneNumber];
            }
        }

        throw new ProviderRuntimeException('Phone data is not set.');
    }

    /**
     * Set phone data
     *
     * @param       $phoneNumber
     * @param array $phoneData
     */
    public function setPhoneData($phoneNumber, array $phoneData)
    {
        $this->phoneData[$phoneNumber] = $phoneData;
    }

    /**
     * @param $phoneNumber
     */
    public function deletePhoneData($phoneNumber)
    {
        if (array_key_exists($phoneNumber, $this->phoneData)) {
            unset($this->phoneData[$phoneNumber]);
        }
    }
    /**
     * @param $phoneNumber
     */
    public function deleteSms($phoneNumber)
    {
        if (array_key_exists($phoneNumber, $this->smsStorage)) {
            unset($this->smsStorage[$phoneNumber]);
        }
    }

    /**
     * @param $paramName
     * @return mixed|null
     */
    public function getConfig($paramName = null)
    {
        if (!is_null($paramName)) {
            return $this->config[$paramName] ?? null;
        } else {
            return $this->config;
        }
    }

    /**
     * @param array $config
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    public function setConfigKey($key, $value)
    {
        $this->config[$key] = $value;
    }
}

