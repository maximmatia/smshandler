<?php

namespace SmsHandler\Provider;

use GuzzleHttp\Exception\GuzzleException;
use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Util\Response\SmsAcktiwatorRuResponse;
use SmsHandler\Util\Response\SmsActivateRuResponse;
use SmsHandler\Wrapper\AbstractWrapper;
use SmsHandler\Wrapper\GoogleCom\GoogleCom_SmsAcktiwatorRuProviderWrapper;
use SmsHandler\Wrapper\LinkedinCom\LinkedinCom_SmsAcktiwatorRuProviderWrapper;
use SmsHandler\Wrapper\TwitterCom\TwitterCom_SmsAcktiwatorRuProviderWrapper;
use SmsHandler\Wrapper\VKCom\VKCOM_SmsHubOrgProviderWrapper;

class SmsAcktiwatorRuProvider extends AbstractProvider
{
    /**
     * @param       $serviceDomain
     * @param array $options
     *
     * @return AbstractWrapper
     * @throws ConfigException
     */
    public function __invoke($serviceDomain, array $options = []): AbstractWrapper
    {
        $serviceDomain = str_replace('www.', '', strtolower($serviceDomain));
        if ($serviceDomain == 'vk.com') {
            return new VKCOM_SmsHubOrgProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'linkedin.com') {
            return new LinkedinCom_SmsAcktiwatorRuProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'twitter.com') {
            return new TwitterCom_SmsAcktiwatorRuProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'google.com') {
            return new GoogleCom_SmsAcktiwatorRuProviderWrapper($this, $options);
        }


        throw new ConfigException('Provider is not supported.');
    }

    /**
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function getBalance()
    {
        $r = $this->httpClient->request('https://sms-acktiwator.ru/api/getbalance/' . $this->getConfig('apiKey'))
            ->getResponse(new SmsAcktiwatorRuResponse());

        if (strlen($r->getBody()) > 0 and !$r->isJson()) {
            return $r->getBody();
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param array $options
     *
     * @return mixed
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function getNumberAmount(array $options = [])
    {
        $request = $this->httpClient->request(
            'https://sms-acktiwator.ru/api/numbersstatus/' . $this->getConfig('apiKey')
        );
        foreach ($options as $k => $v) {
            $request->addQuery($k, $v);
        }
        $r = $request->getResponse(new SmsAcktiwatorRuResponse());

        if ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isJson()) {
            return $r->parseJson(1);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }


    /**
     * @param array $options
     *
     * @return mixed
     * @throws ConfigException
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function fetchNumberRequest(array $options)
    {
        if (!array_key_exists('serviceId', $options)) {
            throw new ConfigException('serviceId is not set');
        }

        $serviceId = $options['serviceId'];
        unset($options['serviceId']);

        $request = $this->httpClient->request('https://sms-acktiwator.ru/api/getnumber/' . $this->getConfig('apiKey'))
            ->addQuery('id', $serviceId);
        foreach ($options as $k => $v) {
            $request->addQuery($k, $v);
        }
        $r = $request->getResponse(new SmsAcktiwatorRuResponse());

        if ($r->isNoNumbers()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_NUMBERS);
        } elseif ($r->isNoBalance()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_BALANCE);
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isJson()) {
            $pr = $r->parseJson(1);

            $this->deletePhoneData($pr['number']);
            $this->setPhoneData($pr['number'], [
                'phoneId' => $pr['id'],
                'phoneNumber' => $pr['number'],
                'serviceId' => $serviceId,
            ]);

            return $pr['number'];
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function getSmsRequest($phoneNumber)
    {
        $r = $this->httpClient->request('https://sms-acktiwator.ru/api/getstatus/' . $this->getConfig('apiKey'))
            ->addQuery('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsAcktiwatorRuResponse());

        if ($r->getBody() == 'null') {
            return null; #waiting for sms
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isJson()) {
            $pr = $r->parseJson(1);

            return $this->smsStorage[$phoneNumber] = $pr['small'];
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function releaseNumber($phoneNumber): bool
    {
        $r = $this->httpClient->request('https://sms-acktiwator.ru/api/setstatus/' . $this->getConfig('apiKey'))
            ->addQuery('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->addQuery('status', '1')
            ->getResponse(new SmsAcktiwatorRuResponse());

        if ($r->getBody() == 1) {
            return true;
        } elseif ($r->isReleasePhoneError()) {
            return false;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function blockNumber($phoneNumber): bool
    {
        return $this->releaseNumber($phoneNumber);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool|mixed
     * @throws ProviderRuntimeException
     * @throws GuzzleException
     */
    public function resendSms($phoneNumber)
    {
        $r = $this->httpClient->request('https://sms-acktiwator.ru/api/play/' . $this->getConfig('apiKey'))
            ->addQuery('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsAcktiwatorRuResponse());

        if (in_array($r->getBody(), [0, 1])) {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }
}

