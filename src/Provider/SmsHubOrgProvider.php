<?php

namespace SmsHandler\Provider;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Util\Response\SmsActivateRuResponse;
use SmsHandler\Wrapper\AbstractWrapper;
use SmsHandler\Wrapper\GoogleCom\GoogleCom_SmsHubOrgProviderWrapper;
use SmsHandler\Wrapper\HotmailCom\HotmailCom_SmsHubOrgProviderWrapper;
use SmsHandler\Wrapper\LinkedinCom\LinkedinCom_SmsHubOrgProviderWrapper;
use SmsHandler\Wrapper\VKCom\VKCOM_SmsHubOrgProviderWrapper;

class SmsHubOrgProvider extends AbstractProvider
{
    const SUPPORTED_COUNTRIES = [
        'ru' => 0, # russia
        'ua' => 1, # ukraine
        'kz' => 2, # kazakhstan
        'cn' => 3, # china
        'ph' => 4, # philippines
        'mm' => 5, # myanmar,
        'id' => 6, # indonesia,
        'my' => 7, # malaysia,
        'ke' => 8, # kenya,
        'tz' => 9, # tanzania,
        'vn' => 10, # vietnam,
        'kg' => 11, # kirghizstan,
        'us' => 12, # usa,
        'il' => 13, # israel,
        'hk' => 14, # hong kong,
        'pl' => 15, # poland,
        'gb' => 16, # united kingdom,
        'mg' => 17, # madagascar,
        'ng' => 19, # nigeria,
        'mo' => 20, # macao,
        'eg' => 21, # egypt,
        'ie' => 23, # ireland,
        'kh' => 24, # cambodia,
        'la' => 25, # lao,
        'ht' => 26, # haiti,
        'ci' => 27, # ivory coast,
        'gm' => 28, # gambia,
        'rs' => 29, # serbia,
        'ye' => 30, # yemen,
        'za' => 31, # south africa,
        'ro' => 32, # romania,
        'ee' => 34, # estonia,
        'az' => 35, # azerbaijan,
        'ca' => 36, # canada,
        'ma' => 37, # morocco,
        'gh' => 38, # ghana,
        'ar' => 39, # argentina,
        'uz' => 40, # uzbekistan
        'cm' => 41, # cameroon
        'td' => 42, # chad
        'de' => 43, # germany
        'lt' => 44, # lithuania
        'hr' => 45, # croatia
        'iq' => 47, # iraq
        'nl' => 48, # netherlands
        'lv' => 49, # latvia
        'at' => 50, # austria
        'by' => 51, # belarus
        'th' => 52, # thailand
        'sa' => 53, # saudi arabia
        'si' => 59, # slovenia
        'cz' => 53, # czechia
        'nz' => 67, # new zealand
        'gq' => 68, # guinea
        'ml' => 69, # mali
        'mn' => 72, # mongolia
        'br' => 73, # brazil

        # https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes
    ];

    /**
     * @param       $serviceDomain
     * @param array $options
     *
     * @return AbstractWrapper
     * @throws ConfigException
     */
    public function __invoke($serviceDomain, array $options = []): AbstractWrapper
    {
        $serviceDomain = str_replace('www.', '', strtolower($serviceDomain));
        if ($serviceDomain == 'vk.com') {
            return new VKCOM_SmsHubOrgProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'linkedin.com') {
            return new LinkedinCom_SmsHubOrgProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'google.com') {
            return new GoogleCom_SmsHubOrgProviderWrapper($this, $options);
        }elseif ($serviceDomain == 'hotmail.com') {
            return new HotmailCom_SmsHubOrgProviderWrapper($this, $options);
        }

        throw new ConfigException('Provider is not supported.');
    }

    /**
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalance()
    {
        $r = $this->httpClient->request('http://smshub.org/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'getBalance')
            ->getResponse(new SmsActivateRuResponse());

        if (substr_count($r->getBody(), 'ACCESS_BALANCE')) {
            return @explode(':', $r->getBody())[1];
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param array $options
     *
     * @return mixed
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getNumberAmount(array $options = [])
    {
        $r = $this->httpClient->request('http://smshub.org/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'getNumbersStatus')
            ->addPostBulk($options)
            ->getResponse(new SmsActivateRuResponse());

        if ($r->isJson()) {
            return $r->parseJson(1);
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }


    /**
     * @param array $options
     *
     * @return mixed
     * @throws ConfigException
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetchNumberRequest(array $options)
    {
        if (!array_key_exists('serviceId', $options)) {
            throw new ConfigException('serviceId is not set');
        }

        $r = $this->httpClient->request('http://smshub.org/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'getNumber')
            ->addPost('service', $options['serviceId'])
            ->addPostBulk($options)
            ->getResponse(new SmsActivateRuResponse());

        if (substr_count($r->getBody(), 'ACCESS_NUMBER')) {
            [$t, $phoneId, $phoneNumber] = explode(':', $r->getBody());

            $this->deletePhoneData($phoneNumber);
            $this->setPhoneData($phoneNumber, [
                'phoneId'     => $phoneId,
                'phoneNumber' => $phoneNumber,
                'serviceId'   => $options['serviceId'],
            ]);

            return $phoneNumber;
        } elseif ($r->isNoNumbers()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_NUMBERS);
        } elseif ($r->isNoBalance()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_BALANCE);
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSmsRequest($phoneNumber)
    {
        $r = $this->httpClient->request('http://smshub.org/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'getStatus')
            ->addPost('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'STATUS_WAIT_CODE') {
            return null; #waiting for sms
        } elseif (substr_count($r->getBody(), 'STATUS_OK')) {
            return $this->smsStorage[$phoneNumber] = @explode(':', $r->getBody())[1];
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function releaseNumber($phoneNumber): bool
    {
        $r = $this->httpClient->request('http://smshub.org/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'setStatus')
            ->addPost('status', '-1')
            ->addPost('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'ACCESS_CONFIRM_GET' or $r->getBody() == 'ACCESS_CANCEL') {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function blockNumber($phoneNumber): bool
    {
        $r = $this->httpClient->request('http://smshub.org/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'setStatus')
            ->addPost('status', '8')
            ->addPost('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'ACCESS_CANCEL') {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool|mixed
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function resendSms($phoneNumber)
    {
        $r = $this->httpClient->request('http://smshub.org/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'setStatus')
            ->addPost('status', '3')
            ->addPost('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'ACCESS_RETRY_GET') {
            return true;
        } elseif ($r->getBody() == 'BAD_STATUS') {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }
}

