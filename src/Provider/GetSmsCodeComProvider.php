<?php

namespace SmsHandler\Provider;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Provider\GetSmsCodeComProviderHandler\AsiaGetSmsCodeComProvider;
use SmsHandler\Provider\GetSmsCodeComProviderHandler\ChinaGetSmsCodeComProvider;
use SmsHandler\Provider\GetSmsCodeComProviderHandler\GetSmsCodeComProviderTrait;
use SmsHandler\Wrapper\AbstractWrapper;
use SmsHandler\Wrapper\VKCom\VKCOM_GetSmsCodeComProviderWrapper;

#TODO update
class GetSmsCodeComProvider extends AbstractProvider
{
    use GetSmsCodeComProviderTrait;
    protected $handler = [];
    protected $parentConfig;

    /**
     * GetSmsCodeComProviderWrapper constructor.
     *
     * @param array $config
     *
     * @throws ConfigException
     */
    public function __construct(array $config)
    {
        if (!array_key_exists('username', $config)) {
            throw new ConfigException('Username is not set.');
        }
        parent::__construct($config);
        $this->parentConfig = $config;
    }

    /**
     * @param       $serviceDomain
     * @param array $options
     *
     * @return AbstractWrapper
     * @throws ConfigException
     */
    public function __invoke($serviceDomain, array $options = []): AbstractWrapper
    {
        $serviceDomain = str_replace('www.', '', strtolower($serviceDomain));
        if ($serviceDomain == 'vk.com') {
            return new VKCOM_GetSmsCodeComProviderWrapper($this, $options);
        }

        throw new ConfigException('Provider is not supported.');
    }

    /**
     * Get handler by phone
     *
     * @param $phoneNumber
     *
     * @return mixed|AbstractProvider
     * @throws ConfigException
     * @throws ProviderRuntimeException
     */
    protected function getHandler($phoneNumber): AbstractProvider
    {
        $country = $this->getPhoneData($phoneNumber, 'country');

        return $this->getHandlerByCountry($country);
    }

    /**
     * Get handler by country
     *
     * @param $country
     *
     * @return mixed
     * @throws ConfigException
     */
    protected function getHandlerByCountry($country): AbstractProvider
    {
        $handlerClassName = $this->getHandlerClassName($country);
        if (!array_key_exists($handlerClassName, $this->handler)) {
            if (class_exists($handlerClassName)) {
                $this->handler[$handlerClassName] = new $handlerClassName($this->parentConfig);
            } else {
                throw new ConfigException('Handler not found.');
            }
        }

        return $this->handler[$handlerClassName];
    }


    /**
     * Get handler class name
     *
     * @param $countryIso
     *
     * @return string
     * @throws ConfigException
     */
    protected function getHandlerClassName($countryIso)
    {
        if (in_array(strtolower($countryIso), ChinaGetSmsCodeComProvider::SUPPORTED_COUNTRIES)) {
            return ChinaGetSmsCodeComProvider::class;
        } elseif (in_array(strtolower($countryIso), AsiaGetSmsCodeComProvider::SUPPORTED_COUNTRIES)) {
            return AsiaGetSmsCodeComProvider::class;
        }

        throw new ConfigException('Unknown country.');
    }

    /**
     * @param array $options
     *
     * @return mixed
     * @throws ConfigException
     */
    public function fetchNumber(array $options)
    {
        if (!array_key_exists('country', $options)) {
            throw new ConfigException('serviceId is not set');
        }

        $phoneNumber = $this->getHandlerByCountry($options['country'])->fetchNumber($options);
        $this->setPhoneData($phoneNumber, $options);

        return $phoneNumber;
    }

    /**
     * @param $phoneNumber
     *
     * @return mixed
     * @throws ConfigException
     * @throws ProviderRuntimeException
     */
    public function getSmsRequest($phoneNumber)
    {
        return $this->getHandler($phoneNumber)->getSmsRequest($phoneNumber);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ConfigException
     * @throws ProviderRuntimeException
     */
    public function releaseNumber($phoneNumber): bool
    {
        return $this->getHandler($phoneNumber)->releaseNumber($phoneNumber);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ConfigException
     * @throws ProviderRuntimeException
     */
    public function blockNumber($phoneNumber): bool
    {
        return $this->releaseNumber($phoneNumber);
    }

    /**
     * @param $phoneNumber
     *
     * @return mixed|void
     * @throws ProviderRuntimeException
     */
    public function resendSms($phoneNumber)
    {
        throw new ProviderRuntimeException('Unsupported action', ProviderRuntimeException::UNSUPPORTED_ACTION);
    }
}

