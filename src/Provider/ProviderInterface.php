<?php

namespace SmsHandler\Provider;

interface ProviderInterface
{
    /**
     * Get balance
     *
     * @return mixed
     */
    public function getBalance();

    /**
     * Check ability to fetch number
     *
     * @param array $options
     *
     * @return mixed
     */
    public function getNumberAmount(array $options = []);

    /**
     * Fetch number
     *
     * @param array $options
     *
     * @return mixed
     */
    public function fetchNumber(array $options);

    /**
     * Release number
     *
     * @param $phoneNumber
     *
     * @return bool
     */
    public function releaseNumber($phoneNumber): bool;

    /**
     * Block number
     *
     * @param $phoneNumber
     *
     * @return bool
     */
    public function blockNumber($phoneNumber): bool;

    /**
     * Resend sms
     *
     * @param $phoneNumber
     *
     * @return mixed
     */
    public function resendSms($phoneNumber);

    /**
     * Get sms loop
     *
     * @param $phoneNumber
     *
     * @return mixed
     */
    public function getSms($phoneNumber);
}

