<?php

namespace SmsHandler\Provider;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Util\Response\SmsActivateRuResponse;
use SmsHandler\Wrapper\AbstractWrapper;
use SmsHandler\Wrapper\GoogleCom\GoogleCom_DropsmsRuProviderWrapper;
use SmsHandler\Wrapper\LinkedinCom\LinkedinCom_DropsmsRuProviderWrapper;
use SmsHandler\Wrapper\VKCom\VKCOM_DropsmsRuProviderWrapper;

class DropsmsRuProvider extends AbstractProvider
{
    /**
     * @param       $serviceDomain
     * @param array $options
     *
     * @return AbstractWrapper
     * @throws ConfigException
     */
    public function __invoke($serviceDomain, array $options = []): AbstractWrapper
    {
        $serviceDomain = str_replace('www.', '', strtolower($serviceDomain));
        if ($serviceDomain == 'vk.com') {
            return new VKCOM_DropsmsRuProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'linkedin.com') {
            return new LinkedinCom_DropsmsRuProviderWrapper($this, $options);
        } elseif ($serviceDomain == 'google.com') {
            return new GoogleCom_DropsmsRuProviderWrapper($this, $options);
        }

        throw new ConfigException('Provider is not supported.');
    }

    /**
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalance()
    {
        $r = $this->httpClient->request('http://api.dropsms.ru/stubs/handler_api.php')
            ->addQuery('api_key', $this->getConfig('apiKey'))
            ->addQuery('action', 'getBalance')
            ->getResponse(new SmsActivateRuResponse());

        if (substr_count($r->getBody(), 'ACCESS_BALANCE')) {
            return @explode(':', $r->getBody())[1];
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param array $options
     *
     * @return mixed
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getNumberAmount(array $options = [])
    {
        $r = $this->httpClient->request('http://api.dropsms.ru/stubs/handler_api.php')
            ->addQuery('api_key', $this->getConfig('apiKey'))
            ->addQuery('action', 'getNumbersStatus')
            ->addQueryBulk($options)
            ->getResponse(new SmsActivateRuResponse());

        if ($r->isJson()) {
            return $r->parseJson(1);
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }


    /**
     * @param array $options
     *
     * @return mixed
     * @throws ConfigException
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetchNumberRequest(array $options)
    {
        if (!array_key_exists('serviceId', $options)) {
            throw new ConfigException('serviceId is not set');
        }

        $r = $this->httpClient->request('http://api.dropsms.ru/stubs/handler_api.php')
            ->addQuery('api_key', $this->getConfig('apiKey'))
            ->addQuery('action', 'getNumber')
            ->addQuery('service', $options['serviceId'])
            ->addQueryBulk($options)
            ->getResponse(new SmsActivateRuResponse());

        if (substr_count($r->getBody(), 'ACCESS_NUMBER')) {
            list($t, $phoneId, $phoneNumber) = explode(':', $r->getBody());

            $this->deletePhoneData($phoneNumber);
            $this->setPhoneData($phoneNumber, [
                'phoneId'     => $phoneId,
                'phoneNumber' => $phoneNumber,
                'serviceId'   => $options['serviceId'],
            ]);

            return $phoneNumber;
        } elseif ($r->isNoNumbers()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_NUMBERS);
        } elseif ($r->isNoBalance()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_BALANCE);
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSmsRequest($phoneNumber)
    {
        $r = $this->httpClient->request('http://api.dropsms.ru/stubs/handler_api.php')
            ->addQuery('api_key', $this->getConfig('apiKey'))
            ->addQuery('action', 'getStatus')
            ->addQuery('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'STATUS_WAIT_CODE') {
            return null; #waiting for sms
        } elseif (substr_count($r->getBody(), 'STATUS_OK')) {
            return $this->smsStorage[$phoneNumber] = @explode(':', $r->getBody())[1];
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function releaseNumber($phoneNumber): bool
    {
        $r = $this->httpClient->request('http://api.dropsms.ru/stubs/handler_api.php')
            ->addQuery('api_key', $this->getConfig('apiKey'))
            ->addQuery('action', 'setStatus')
            ->addQuery('status', '-1')
            ->addQuery('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'ACCESS_CONFIRM_GET' or $r->getBody() == 'ACCESS_CANCEL') {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function blockNumber($phoneNumber): bool
    {
        $r = $this->httpClient->request('http://api.dropsms.ru/stubs/handler_api.php')
            ->addQuery('api_key', $this->getConfig('apiKey'))
            ->addQuery('action', 'setStatus')
            ->addQuery('status', '8')
            ->addQuery('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'ACCESS_CANCEL') {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool|mixed
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function resendSms($phoneNumber)
    {
        $r = $this->httpClient->request('http://api.dropsms.ru/stubs/handler_api.php')
            ->addQuery('api_key', $this->getConfig('apiKey'))
            ->addQuery('action', 'setStatus')
            ->addQuery('status', '3')
            ->addQuery('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'ACCESS_RETRY_GET') {
            return true;
        } elseif ($r->getBody() == 'BAD_STATUS') {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }
}

