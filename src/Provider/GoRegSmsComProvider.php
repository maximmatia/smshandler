<?php

namespace SmsHandler\Provider;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Util\Response\GoRegSmsComResponse;
use SmsHandler\Wrapper\AbstractWrapper;
use SmsHandler\Wrapper\VKCom\VKCOM_GoRegSmsComProviderWrapper;

class GoRegSmsComProvider extends AbstractProvider
{
    /**
     * @param       $serviceDomain
     * @param array $options
     *
     * @return AbstractWrapper
     * @throws ConfigException
     */
    public function __invoke($serviceDomain, array $options = []): AbstractWrapper
    {
        $serviceDomain = str_replace('www.', '', strtolower($serviceDomain));
        if ($serviceDomain == 'vk.com') {
            return new VKCOM_GoRegSmsComProviderWrapper($this, $options);
        }

        throw new ConfigException('Provider is not supported.');
    }

    /**
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalance()
    {
        return null;
    }

    /**
     * @param array $options
     *
     * @return mixed
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getNumberAmount(array $options = [])
    {
        $r = $this->httpClient->request('https://goreg-sms.com/api/legacy')
            ->addPost('key', $this->getConfig('apiKey'))
            ->addPost('action', 'GET_SERVICES')
            ->addPostBulk($options)
            ->getResponse(new GoRegSmsComResponse());

        if ($r->isJson()) {
            return $r->parseJson(1);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }


    /**
     * @param array $options
     *
     * @return mixed
     * @throws ConfigException
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetchNumberRequest(array $options)
    {
        if (!array_key_exists('serviceId', $options)) {
            throw new ConfigException('serviceId is not set');
        } elseif (!array_key_exists('country', $options)) {
            throw new ConfigException('country is not set');
        }

        $r = $this->httpClient->request('https://goreg-sms.com/api/legacy')
            ->addPost('key', $this->getConfig('apiKey'))
            ->addPost('action', 'GET_NUMBER')
            ->addPost('service', $options['serviceId'])
            ->addPostBulk($options)
            ->getResponse(new GoRegSmsComResponse())
            ->parseJson(1);

        if (isset($r['status']) AND $r['status'] == 'SUCCESS') {
            $this->deletePhoneData($r['number']);
            $this->setPhoneData($r['number'], [
                'phoneId'     => $r['activationId'],
                'phoneNumber' => $r['number'],
                'serviceId'   => $options['serviceId'],
            ]);

            return $r['number'];
        } elseif ($r->isNoNumbers()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_NUMBERS);
        } elseif ($r->isNoBalance()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_BALANCE);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);

    }

    /**
     * @param $phoneNumber
     *
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSmsRequest($phoneNumber)
    {
        $r = $this->httpClient->request('https://goreg-sms.com/api/legacy')
            ->addPost('key', $this->getConfig('apiKey'))
            ->addPost('action', 'GET_SMS')
            ->addPost('smsIds', [$this->getPhoneData($phoneNumber, 'phoneId')])
            ->getResponse(new GoRegSmsComResponse())
            ->parseJson(1);;

        if (isset($r['status']) AND $r['status'] == 'SUCCESS' AND $r['sms'][0]['text'] == 'no text') {
            return null; #waiting for sms
        } elseif (isset($r['status']) AND $r['status'] == 'SUCCESS' AND $r['sms'][0]['text'] !== 'no text') {
            return $this->smsStorage[$phoneNumber] = $r['sms'][0]['text'];
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function releaseNumber($phoneNumber): bool
    {
        $r = $this->httpClient->request('https://goreg-sms.com/api/legacy')
            ->addPost('key', $this->getConfig('apiKey'))
            ->addPost('action', 'FINISH_ACTIVATION')
            ->addPost('status', '4')
            ->addPost('activationId', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new GoRegSmsComResponse())
            ->parseJson(1);

        if (isset($r['status']) AND $r['status'] == 'SUCCESS') {
            return true;
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function blockNumber($phoneNumber): bool
    {
        $r = $this->httpClient->request('https://goreg-sms.com/api/legacy')
            ->addPost('key', $this->getConfig('apiKey'))
            ->addPost('action', 'FINISH_ACTIVATION')
            ->addPost('status', '1')
            ->addPost('activationId', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new GoRegSmsComResponse())
            ->parseJson(1);

        if (isset($r['status']) AND $r['status'] == 'SUCCESS') {
            return true;
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool|mixed
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function resendSms($phoneNumber)
    {
        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }
}

