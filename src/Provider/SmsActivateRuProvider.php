<?php

namespace SmsHandler\Provider;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Util\Response\SmsActivateRuResponse;
use SmsHandler\Wrapper\AbstractWrapper;
use SmsHandler\Wrapper\VKCom\VKCOM_SmsActivateRuProviderWrapper;

class SmsActivateRuProvider extends AbstractProvider
{
    /**
     * @param       $serviceDomain
     * @param array $options
     *
     * @return AbstractWrapper
     * @throws ConfigException
     */
    public function __invoke($serviceDomain, array $options = []): AbstractWrapper
    {
        $serviceDomain = str_replace('www.', '', strtolower($serviceDomain));
        if ($serviceDomain == 'vk.com') {
            return new VKCOM_SmsActivateRuProviderWrapper($this, $options);
        }

        throw new ConfigException('Provider is not supported.');
    }

    /**
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalance()
    {
        $r = $this->httpClient->request('http://sms-activate.ru/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'getBalance')
            ->getResponse(new SmsActivateRuResponse());

        if (substr_count($r->getBody(), 'ACCESS_BALANCE')) {
            return @explode(':', $r->getBody())[1];
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param array $options
     *
     * @return mixed
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getNumberAmount(array $options = [])
    {
        $r = $this->httpClient->request('http://sms-activate.ru/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'getNumbersStatus')
            ->addPostBulk($options)
            ->getResponse(new SmsActivateRuResponse());

        if ($r->isJson()) {
            return $r->parseJson(1);
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }


    /**
     * @param array $options
     *
     * @return mixed
     * @throws ConfigException
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetchNumberRequest(array $options)
    {
        if (!array_key_exists('serviceId', $options)) {
            throw new ConfigException('serviceId is not set');
        }

        $r = $this->httpClient->request('http://sms-activate.ru/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'getNumber')
            ->addPost('service', $options['serviceId'])
            ->addPostBulk($options)
            ->getResponse(new SmsActivateRuResponse());

        if (substr_count($r->getBody(), 'ACCESS_NUMBER')) {
            list($t, $phoneId, $phoneNumber) = explode(':', $r->getBody());

            $this->deletePhoneData($phoneNumber);
            $this->setPhoneData($phoneNumber, [
                'phoneId'     => $phoneId,
                'phoneNumber' => $phoneNumber,
                'serviceId'   => $options['serviceId'],
            ]);

            return $phoneNumber;
        } elseif ($r->isNoNumbers()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_NUMBERS);
        } elseif ($r->isNoBalance()) {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_BALANCE);
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);

    }

    /**
     * @param $phoneNumber
     *
     * @return mixed|null
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSmsRequest($phoneNumber)
    {
        $r = $this->httpClient->request('http://sms-activate.ru/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'getStatus')
            ->addPost('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'STATUS_WAIT_CODE') {
            return null; #waiting for sms
        } elseif (substr_count($r->getBody(), 'STATUS_OK')) {
            return $this->smsStorage[$phoneNumber] = @explode(':', $r->getBody())[1];
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function releaseNumber($phoneNumber): bool
    {
        $r = $this->httpClient->request('http://sms-activate.ru/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'setStatus')
            ->addPost('status', '-1')
            ->addPost('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'ACCESS_CONFIRM_GET' OR $r->getBody() == 'ACCESS_CANCEL') {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function blockNumber($phoneNumber): bool
    {
        $r = $this->httpClient->request('http://sms-activate.ru/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'setStatus')
            ->addPost('status', '8')
            ->addPost('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'ACCESS_CANCEL') {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool|mixed
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function resendSms($phoneNumber)
    {
        $r = $this->httpClient->request('http://sms-activate.ru/stubs/handler_api.php')
            ->addPost('api_key', $this->getConfig('apiKey'))
            ->addPost('action', 'setStatus')
            ->addPost('status', '3')
            ->addPost('id', $this->getPhoneData($phoneNumber, 'phoneId'))
            ->getResponse(new SmsActivateRuResponse());

        if ($r->getBody() == 'ACCESS_RETRY_GET') {
            return true;
        } elseif ($r->isRequestError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::BAD_OPTIONS);
        } elseif ($r->isInternalError()) {
            throw new ProviderRuntimeException($r->getBody(), ProviderRuntimeException::INTERNAL_ERROR);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }
}

