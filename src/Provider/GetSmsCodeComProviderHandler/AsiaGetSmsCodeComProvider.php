<?php

namespace SmsHandler\Provider\GetSmsCodeComProviderHandler;

use SmsHandler\Exception\ConfigException;
use SmsHandler\Exception\ProviderRuntimeException;
use SmsHandler\Provider\AbstractProvider;

class AsiaGetSmsCodeComProvider extends AbstractProvider
{
    use GetSmsCodeComProviderTrait;

    const SUPPORTED_COUNTRIES = [

        'malaysia',
        'my',
        'philippines',
        'ph',
        'cambodia',
        'kh',
        'myanmar',
        'mm',
        'vietnam',
        'vn',
        'indonesia',
        'id',
        'hongkong',
        'hk',
        'macao',
        'mo',
        'tailand',
        'th',
        'brazil',
        'br',
        'egypt',
        'eg',
    ];


    /**
     * @param array $options
     *
     * @return mixed|string
     * @throws ConfigException
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetchNumber(array $options)
    {
        if (!array_key_exists('serviceId', $options)) {
            throw new ConfigException('serviceId is not set');
        }

        $result = $this->httpClient->request('POST', 'http://www.getsmscode.com/vndo.php', [
            'form_params' => [
                'action'   => 'getmobile',
                'username' => $this->getConfig('username'),
                'token'    => $this->getConfig('apiKey'),
                'pid'      => $options['serviceId'],
                'cocode'   => $options['country'],
            ],
        ])->getBody()->getContents();

        if (is_numeric($result)) {
            # $result is phoneNumber
            $this->deletePhoneData($result);
            $this->setPhoneData($result, [
                'number'    => $result,
                'serviceId' => $options['serviceId'],
                'country'   => $options['country'],
            ]);

            return $result;
        } elseif (strtolower($result) == 'message|unavailable') {
            throw new ProviderRuntimeException('No phone numbers available.', ProviderRuntimeException::NO_NUMBERS);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);

    }

    /**
     * @param $phoneNumber
     *
     * @return bool|mixed|null|string
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSmsRequest($phoneNumber)
    {
        $result = $this->httpClient->request('POST', 'http://www.getsmscode.com/vndo.php', [
            'form_params' => [
                'action'   => 'getsms',
                'username' => $this->getConfig('username'),
                'token'    => $this->getConfig('apiKey'),
                'pid'      => $this->getPhoneData($phoneNumber, 'serviceId'),
                'mobile'   => $phoneNumber,
                'cocode'   => $this->getPhoneData($phoneNumber, 'country'),
            ],
        ])->getBody()->getContents();

        if (strtolower($result) == 'message|not receive') {
            return null; #waiting for sms
        } elseif ($result[0] == 1) {
            return substr($result, 2);
        }

        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return bool
     * @throws ProviderRuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function releaseNumber($phoneNumber): bool
    {
        $result = $this->httpClient->request('POST', 'http://www.getsmscode.com/vndo.php', [
            'form_params' => [
                'action'   => 'addblack',
                'username' => $this->getConfig('username'),
                'token'    => $this->getConfig('apiKey'),
                'pid'      => $this->getPhoneData($phoneNumber, 'serviceId'),
                'mobile'   => $phoneNumber,
                'cocode'   => $this->getPhoneData($phoneNumber, 'country'),
            ],
        ])->getBody()->getContents();

        if (strtolower($result) === 'message|had add black list') {
            return true;
        }
        throw new ProviderRuntimeException('Unknown response', ProviderRuntimeException::BAD_RESPONSE);
    }

    /**
     * @param $phoneNumber
     *
     * @return mixed|void
     * @throws ProviderRuntimeException
     */
    public function resendSms($phoneNumber)
    {
        throw new ProviderRuntimeException('Unsupported action', ProviderRuntimeException::UNSUPPORTED_ACTION);
    }
}

