<?php
/**
 * Created by PhpStorm.
 * User: Maxim
 * Date: 11.05.2017
 * Time: 18:44
 */

namespace SmsHandler\Exception;


class ProviderRuntimeException extends SmsHandlerException
{
    const BAD_RESPONSE = 1;
    const BAD_OPTIONS = 2;
    const NO_NUMBERS = 3;
    const NO_SMS = 4;
    const NO_BALANCE = 5;
    const UNSUPPORTED_ACTION = 6;
    const INTERNAL_ERROR = 7;
}